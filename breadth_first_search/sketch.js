let data;
let graph;
let dropdownStart;
let dropdownEnd;

function preload(){
    data = loadJSON("data.json");
}

function setup(){
    noCanvas();
    dropdownStart = createSelect();
    createElement('a', '&nbsp;&nbsp;');
    dropdownEnd = createSelect();
    createElement('a', '&nbsp;&nbsp;');

    let clickMe = createButton("Perform Search");
    clickMe.mousePressed(breadth_first_search);
    
    graph = new Graph;

    let movies = data.movies;
    movies.forEach(movie => {
        let title = movie.title;
        let cast = movie.cast;

        let movieNode = new Node(title);
        graph.addNode(movieNode);

        cast.forEach(actor => {
            let actorNode = graph.getNode(actor);
            if(actorNode == undefined){
                actorNode = new Node(actor);
                dropdownStart.option(actor);
                dropdownEnd.option(actor);
            }
            
            graph.addNode(actorNode);
            movieNode.addEdge(actorNode);
        });
    });
}

function breadth_first_search(){
    graph.reset();
    let start = graph.setStart(dropdownStart.value());
    let end = graph.setEnd(dropdownEnd.value());
    let queue = [];

    start.searched = true;
    queue.push(start);

    while(queue.length > 0){
        let current = queue.shift();
        if(current === end){
            break;
        }

        let edges = current.edges;
        edges.forEach(neighbor => {
            if(!neighbor.searched){
                neighbor.searched = true;
                neighbor.parent = current;
                queue.push(neighbor);
            }
        });
    }

    let path = [];
    path.push(end);
    let next = end.parent;
    while(next != null){
        path.push(next);
        next = next.parent;
    }

    let txt = '';
    for(let i = path.length - 1; i >= 0; i--){
        let n = path[i];
        txt += n.value;
        if(i != 0){
            txt += ' --> ';
        }
    }

    createP(txt);
}