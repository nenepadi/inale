function Graph(){
    this.nodes = [];
    this.graph = {};
    this.start = null;
    this.end = null;
}

Graph.prototype.addNode = function(node){
    this.nodes.push(node);

    let title = node.value;
    this.graph[title] = node;
}

Graph.prototype.getNode = function(node){
    let result = this.graph[node];
    return result;
}

Graph.prototype.reset = function(){
    this.nodes.forEach(node => {
        node.searched = false;
        node.parent = null;
    });
}

Graph.prototype.setEnd = function(node){
    this.end = this.graph[node];
    return this.end;
}

Graph.prototype.setStart = function(node){
    this.start = this.graph[node];
    return this.start;
}