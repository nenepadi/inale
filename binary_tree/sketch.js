var tree;

function setup() {
    createCanvas(1300, 768);
    background(51);

    tree = new Tree();
    for (var i = 0; i < 20; i++) {
        tree.addValue(floor(random(0, 100)));
    }

    tree.traverse();
    var result = tree.search(50);
    if (result === null) {
        console.log("not found");
    } else {
        console.log(result);
    }
}