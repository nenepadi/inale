function Node(val, x, y) {
    this.value = val;
    this.left = null;
    this.right = null;

    this.distance = 2;

    this.x = x;
    this.y = y;
}

Node.prototype.addNode = function(n) {
    if (n.value < this.value) {
        if (this.left === null) {
            this.left = n;
            this.left.x = this.x - (width / pow(2, n.distance));
            this.left.y = this.y + (height / 20);
        } else {
            n.distance++;
            this.left.addNode(n);
        }
    } else if (n.value > this.value) {
        if (this.right === null) {
            this.right = n;
            this.right.x = this.x + (width / pow(2, n.distance));
            this.right.y = this.y + (height / 20);
        } else {
            n.distance++;
            this.right.addNode(n);
        }
    }
};

Node.prototype.visit = function(parent) {
    if (this.left !== null) {
        this.left.visit(this);
    }

    console.log(this.value);

    stroke(100);
    line(parent.x, parent.y, this.x, this.y);

    fill(0);
    stroke(255);
    ellipse(this.x, this.y, 24, 24);

    fill(255);
    noStroke();
    textAlign(CENTER);
    textSize(12);
    text(this.value, this.x, this.y + 4);

    if (this.right !== null) {
        this.right.visit(this);
    }
};

Node.prototype.search = function(val) {
    if (this.value == val) {
        return this;
    } else if (val < this.value && this.left !== null) {
        return this.left.search(val);
    } else if (val > this.value && this.right !== null) {
        return this.right.search(val);
    }

    return null;
}